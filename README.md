* Author: Mark Bain

* Date:   2016-03-03


## Requirements ##

The script requires that a suitable sudo account exists on each of the remote servers with NOPASSWD set e.g:

%sudo	ALL=(ALL:ALL) NOPASSWD:ALL

It also requires the ssh public keys to be installed on the remote servers:

* ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa

* ssh-copy-id account@server1

* ssh-copy-id account@server2


The commands to be run can then be placed in a command file and all patches in a patches directory. 

The command can then be run as follows:

```
run_automation.sh -a account -s"server list" -c command_file -p patches_folder
```


## Example Case ##
In the example case two Oracle VM Virtual boxes are set up, each created using a current Ubuntu ISO (http://www.ubuntu.com/download/desktop). The next steps are:

* ddd the IP addresses of the servers into the local /etc/hosts file

* use ssh-copy-id to export the public ssh keys

* amend the remote /etc/sudoers file to allow NOPASSWD for the admin account to be used


Next:

* place all patches to be run in a new directory

* create a file containing any commands to be run



The command can then be run:
```
run_automation.sh -a bainm -s"server1 server2" -c /home/mark/bin/command_file.sh -p ~/patches
```