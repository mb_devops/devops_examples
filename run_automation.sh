<<COMMENTS
Author: Mark Bain
Date:   2016-03-03

The script requires that a suitable sudo account exists on each of the remote servers with NOPASSWD set e.g:
%sudo	ALL=(ALL:ALL) NOPASSWD:ALL

It also requires the ssh public keys to be installed on the remote servers:
ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa

ssh-copy-id bainm@server1
ssh-copy-id bainm@server2

The commands to be run can then be placed in a command file and all patches in a patches directory. 
The command can then be run as follows:

run_automation.sh -a bainm -s"server1 server2" -c /home/mark/bin/command_file.sh -p ~/patches

COMMENTS


. functions.sh

run_automation "$@"