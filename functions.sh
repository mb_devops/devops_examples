function run_automation_usage {
cat <<STOP
-a admin account
-p patch directory
-s server list
-c command file
STOP
}

function run_automation {
    #Get inputs
    while getopts a:c:p:s: opt
    do
        case $opt in
            a) 
                ACCOUNT="${OPTARG}"
                ;;
            c)
                COMMAND_FILE="${OPTARG}"
                ;;
            p)
                PATCH_DIR="${OPTARG}"
                ;;
            s)
                 SERVERS="${OPTARG}"
                ;;
            *)
                echo "Invalid option: -$opt $OPTARG" >&2
                ;;
        esac
    done
    
    #Check the inputs
    [ "${ACCOUNT}" == "" ] && {
        echo "Admin account required"
        ERRORFOUND=1
    }
    
    [ "${SERVERS}" == "" ] && {
        echo "Server list required"
        ERRORFOUND=1
    }
    
    [ ! -f "${COMMAND_FILE:-undefined}" ] && {
        echo "Command file (${COMMAND_FILE:-undefined}) must exist and be readable"
        ERRORFOUND=1
    }
    
    [  ${ERRORFOUND:-0} -eq 1 ] && {
        run_automation_usage
        exit
    }
    
    #process any patches on the servers
    [ -d "${PATCH_DIR:-undefined}" ] && {
        echo "Applying patches"
        
        REMOTE_DIR=/tmp/patches
        for SERVER in ${SERVERS}
        do
            ssh ${ACCOUNT}@${SERVER} "mkdir -p ${REMOTE_DIR}"
            ssh ${ACCOUNT}@${SERVER} "rm -rf  ${REMOTE_DIR}/*"
        
            cd ${PATCH_DIR} 
            read FILES <<< $(ls)
            for F in ${FILES}
            do
                scp -q $F ${ACCOUNT}@${SERVER}:${REMOTE_DIR}/
                ssh ${ACCOUNT}@${SERVER} "sudo patch < ${REMOTE_DIR}/$F"
            done
        done
    } 
   
    #Process the command file on the servers
    for SERVER in ${SERVERS}
    do
       echo ${SERVER} 
       read REMOTE_FILE <<< $(echo "/tmp/$(basename ${COMMAND_FILE})")
       scp -q ${COMMAND_FILE} ${ACCOUNT}@${SERVER}:${REMOTE_FILE}
       ssh ${ACCOUNT}@${SERVER} . ${REMOTE_FILE}
    done
    
}